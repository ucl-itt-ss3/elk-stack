# ELK Stack

Amazing ELK Stack Project!

Guide used to make script:
https://linuxconfig.org/install-elk-on-ubuntu-18-04-bionic-beaver-linux

Packer is written with JSON and is able to automate the installation of virtual instances, without the user having to manually set it up every time. This packer script allows the user to create a virtual instance w/ Elk-Stack. The commands for installing Elk-Stack are located in the runme.sh file, which the packer script reads every time it is run.

**How To:**

The Packer script must be run with the correct information added. The source path for the Ubuntu 18.04 image that the script uses and the output directory for produces virtual instance to be located, must both be set up correctly to fit the users needs. 

In order to access the virtual instance securely, SSH must be set up. This is being done with a user and a password. 

```json
"source_path":"path_for_the_ubuntu.18.04_image",
"output_directory":"path_for_virtual_instance",
"ssh_username":"your_ssh_username",
"ssh_password":"your_secure_ssh_password"
```

Kibana requires a user and a password to work correctly. A temporary user *admin* and password *somepasswd* is in the runme.sh file. These should be changed for security reasons.


**Test:** 
- [x] packer.json
- [x] runme.sh