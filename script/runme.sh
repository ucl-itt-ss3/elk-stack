#!/bin/sh

# Ubuntu 18.04
# 4 GB of Ram used!
# Run as user w/ sudo priv.

# USER AND PASSWORD FOR KIBANA 
USR="admin"
PASSWD="somepasswd"

apt-get update

echo "USER COMMENT: ADDING CORRECT REPOSITORIES"
add-apt-repository main
add-apt-repository universe
add-apt-repository restricted
add-apt-repository multiverse

echo "USER COMMENT: INSTALLING JAVA, WGET AND NGNIX"
apt install -y openjdk-8-jre apt-transport-https wget nginx

# wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -
wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | apt-key add -

touch /etc/apt/sources.list.d/elastic.list
echo 'deb https://artifacts.elastic.co/packages/6.x/apt stable main' > /etc/apt/sources.list.d/elastic.list

apt-get update

echo "USER COMMENT: INSTALLING SYSTEMD"
apt-get install -y systemd --allow-unauthenticated

echo "USER COMMENT: INSTALLING ELASTICSEARCH AND KIBANA"
apt install -y elasticsearch kibana --allow-unauthenticated

sed -i 's/#server.host: \"localhost\"/server.host: \"localhost\"/' /etc/kibana/kibana.yml 

echo "USER COMMENT: RESTART KIBANA"
systemctl restart kibana
echo "USER COMMENT: START ELASTICSEARCH"
systemctl start elasticsearch

# echo "$USR:`openssl passwd -apr1 $PASSWD`" | sudo tee -a /etc/nginx/htpasswd.kibana
echo "$USR:`openssl passwd -apr1 $PASSWD`" | tee -a /etc/nginx/htpasswd.kibana

touch /etc/nginx/sites-available/kibana
echo "server {" >> /etc/nginx/sites-available/kibana
echo "        listen 80;" >> /etc/nginx/sites-available/kibana
echo "        server_name $HOSTNAME;" >> /etc/nginx/sites-available/kibana
echo "        auth_basic \"Restricted Access\";" >> /etc/nginx/sites-available/kibana
echo "        auth_basic_user_file /etc/nginx/htpasswd.kibana;" >> /etc/nginx/sites-available/kibana
echo "        location / {" >> /etc/nginx/sites-available/kibana
echo "            proxy_pass http://localhost:5601;" >> /etc/nginx/sites-available/kibana
echo "            proxy_http_version 1.1;" >> /etc/nginx/sites-available/kibana
echo "            proxy_set_header Upgrade \$http_upgrade;" >> /etc/nginx/sites-available/kibana
echo "            proxy_set_header Connection \"upgrade\";" >> /etc/nginx/sites-available/kibana
echo "            proxy_set_header Host \$host;" >> /etc/nginx/sites-available/kibana
echo "            proxy_cache_bypass \$http_upgrade;" >> /etc/nginx/sites-available/kibana     
echo "       }" >> /etc/nginx/sites-available/kibana
echo "    }" >> /etc/nginx/sites-available/kibana


rm /etc/nginx/sites-enabled/default
ln -s /etc/nginx/sites-available/kibana /etc/nginx/sites-enabled/kibana

echo "USER COMMENT: RESTART NGNIX"
systemctl restart nginx

echo "USER COMMENT: INSTALLING LOGSTASH"
apt install -y logstash --allow-unauthenticated

# xdg-open http://localhost
